wb2_6_200803_var_and_covar_2-day-filt.asc

Instrument: RCM11_451
Variance of u anomaly: 103.919522
Variance of v anomaly: 355.076768
Variance of t anomaly: 0.924458
Variance of p anomaly: 573.709745
Co-Variance of u and v anomalies: 35.380814
Co-Variance of u and t anomalies: 0.170148
Co-Variance of u and p anomalies: 38.951591
Co-Variance of v and t anomalies: -2.870029
Co-Variance of v and p anomalies: 302.876868
Co-Variance of t and p anomalies: -12.898877
Instrument: MC_5773
Variance of t anomaly: 0.842173
Variance of p anomaly: 588.053298
Co-variance of u anomaly (from current meter) and t anomaly (from MicroCAT): 0.072575
Co-variance of u anomaly (from current meter) and p anomaly (from MicroCAT): 39.948857
Co-variance of v anomaly (from current meter) and t anomaly (from MicroCAT): -3.519689
Co-variance of v anomaly (from current meter) and p anomaly (from MicroCAT): 305.273210
Co-variance of t anomaly (from current meter) and t anomaly (from MicroCAT): 0.867043
Co-variance of p anomaly (from current meter) and p anomaly (from MicroCAT): 580.400394

Instrument: RCM11_305
Variance of u anomaly: 72.967318
Variance of v anomaly: 371.270108
Variance of t anomaly: 0.403937
Variance of p anomaly: 521.701979
Co-Variance of u and v anomalies: 77.260214
Co-Variance of u and t anomalies: -1.069351
Co-Variance of u and p anomalies: 78.079873
Co-Variance of v and t anomalies: -3.783537
Co-Variance of v and p anomalies: 302.019594
Co-Variance of t and p anomalies: -9.170304
Instrument: MC_5774
Variance of t anomaly: 0.321322
Variance of p anomaly: 516.031283
Co-variance of u anomaly (from current meter) and t anomaly (from MicroCAT): -1.029702
Co-variance of u anomaly (from current meter) and p anomaly (from MicroCAT): 77.859790
Co-variance of v anomaly (from current meter) and t anomaly (from MicroCAT): -3.632005
Co-variance of v anomaly (from current meter) and p anomaly (from MicroCAT): 300.187193
Co-variance of t anomaly (from current meter) and t anomaly (from MicroCAT): 0.357617
Co-variance of p anomaly (from current meter) and p anomaly (from MicroCAT): 518.283008

Instrument: RCM11_306
Variance of u anomaly: 69.656746
Variance of v anomaly: 336.416322
Variance of t anomaly: 0.136608
Variance of p anomaly: 504.056998
Co-Variance of u and v anomalies: 72.856140
Co-Variance of u and t anomalies: -0.333509
Co-Variance of u and p anomalies: 90.813611
Co-Variance of v and t anomalies: -0.630773
Co-Variance of v and p anomalies: 267.718060
Co-Variance of t and p anomalies: -4.595274

Instrument: RCM11_445
Variance of u anomaly: 20.959762
Variance of v anomaly: 120.151680
Variance of t anomaly: 0.252837
Variance of p anomaly: 346.813090
Co-Variance of u and v anomalies: 35.507660
Co-Variance of u and t anomalies: -0.779601
Co-Variance of u and p anomalies: 53.555056
Co-Variance of v and t anomalies: -0.853341
Co-Variance of v and p anomalies: 100.095588
Co-Variance of t and p anomalies: -5.960588

Instrument: RCM11_448
Variance of u anomaly: 16.571987
Variance of v anomaly: 51.030511
Variance of t anomaly: 0.016150
Variance of p anomaly: 204.749619
Co-Variance of u and v anomalies: 11.374312
Co-Variance of u and t anomalies: 0.000523
Co-Variance of u and p anomalies: 22.266726
Co-Variance of v and t anomalies: -0.121202
Co-Variance of v and p anomalies: 41.274883
Co-Variance of t and p anomalies: -0.344701

Instrument: RCM11_449
Variance of u anomaly: 49.185283
Variance of v anomaly: 59.875374
Variance of t anomaly: 0.005498
Variance of p anomaly: 113.119610
Co-Variance of u and v anomalies: 37.343579
Co-Variance of u and t anomalies: -0.010212
Co-Variance of u and p anomalies: 12.896610
Co-Variance of v and t anomalies: -0.011078
Co-Variance of v and p anomalies: 14.976347
Co-Variance of t and p anomalies: 0.036272

Instrument: RCM11_450
Variance of u anomaly: 19.257533
Variance of v anomaly: 39.316116
Variance of t anomaly: 0.010272
Variance of p anomaly: 20.693727
Co-Variance of u and v anomalies: 16.282646
Co-Variance of u and t anomalies: -0.024028
Co-Variance of u and p anomalies: 1.031571
Co-Variance of v and t anomalies: 0.097439
Co-Variance of v and p anomalies: 2.149108
Co-Variance of t and p anomalies: 0.054440